package main

import (

  "os"
  "fmt"
  "log"
  "bytes"
  "strconv"
  "image"
  "image/gif"
  "github.com/andybons/gogif"
  "github.com/kbinani/screenshot"
)

func main() {

  if len(os.Args) < 3 || len(os.Args) > 3 {
    log.Print("\033[38:2:200:0:0mIncorrect amount of arguments, try ./giffy -l <NUMBEROFFRAMES>")
    os.Exit(1)
  }else if os.Args[1] != "-l" {
    log.Print("\033[38:2:200:0:0mIncorrect argument flag, try ./giffy -l <NUMBEROFFRAMES>")
    os.Exit(1)
  }
  var giffyLog bytes.Buffer
  log.New(&giffyLog, "GiffyLog", log.Llongfile)
  log.Print("\033[38:2:200:200:0mLet us gif.\033[0m")

  numFrames, err := strconv.Atoi(os.Args[2])
  if err != nil {
    panic(err)
  }
  var firstRun bool
  var file *os.File
  var gifOpt gif.Options
  gifOpt.NumColors = 256
  gifOpt.Quantizer = nil
  gifOpt.Drawer = nil
  firstRun = true
  var subimages []image.Image // RGBA, etc. images from somewhere else


  for i := 0;i < numFrames;i++ {
    bounds := screenshot.GetDisplayBounds(0)
    img, err := screenshot.CaptureRect(bounds)
    if err != nil {
      panic(err)
    }
    subimages = append(subimages, img)

    if firstRun {
      fileName := fmt.Sprintf("%d_%dx%d.gif", i, bounds.Dx(), bounds.Dy())
      file, err = os.Create("files/"+fileName)
      defer file.Close()
      firstRun = false
    }
  }


  outGif := &gif.GIF{}
  for _, simage := range(subimages) {
    bounds := simage.Bounds()
    palettedImage := image.NewPaletted(bounds, nil)
    quantizer := gogif.MedianCutQuantizer{NumColor: 64}
    quantizer.Quantize(palettedImage, bounds, simage, image.ZP)

    // Add new frame to animated GIF
    outGif.Image = append(outGif.Image, palettedImage)
    outGif.Delay = append(outGif.Delay, 0)
  }
  gif.EncodeAll(file, outGif)



}
